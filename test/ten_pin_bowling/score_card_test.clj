(ns ten-pin-bowling.score-card-test
  (:require [clojure.test :refer :all]
            [ten-pin-bowling.score-card :as sc]))

(deftest initial-score-card
  (testing "Success with two players"
    (let [score-card (sc/initial-score-card "New Game" "Player 1" "Player 2")]
      (is (= "New Game" (:game-name score-card)) "Expected :  Game name to be New Game")
      (is (= 2 (count (:players score-card))) "Expected : Players count 2")))

  (testing "Fail without game name"
    (let [error (Throwable->map (sc/initial-score-card "" "Player 1" "Player 2"))]
      (is (= (:cause error) "Invalid game name")
          "Expected to  fail : Invalid game name")))

  (testing "Fail more than 10 players"
    (let [error (Throwable->map (sc/initial-score-card "New Game"
                                                       "Player 1" "Player 2" "Player 3" "Player 4" "Player 5"
                                                       "Player 6" "Player 7" "Player 8" "Player 9" "Player 10"
                                                       "Player 11"))]
      (is (= (:cause error) "Invalid players list")
          "Expected to  fail : Invalid players list")))

  ;todo: test duplicate player names
  ;todo: test no players
  )


(deftest score-score-card
  (testing "open the match - score the 1st frame of player 1 and move the index to 2nd"
    (let [score-card (-> (sc/initial-score-card "New Game" "Player 1" "Player 2")
                         (sc/score-score-card 4 3))]
      (is (= 1 (:current-player-index score-card)))
      (is (= {:label "4 3", :running-score 7} (-> score-card :players (first) :frames (first))))))

  (testing "score a strike"
    (let [score-card (-> (sc/initial-score-card "New Game" "Player 1")
                         (sc/score-score-card 4 3)          ;1st open frame
                         (sc/score-score-card 10 0)         ;strike
                         )]
      (is (= 0 (:current-player-index score-card)) "only one player, index stay on 0")
      (is (= {:label "X"} (-> score-card :players (first) :frames (last))) "label is X and no running score")))

  (testing "score a spare after a strike"
    (let [score-card (-> (sc/initial-score-card "New Game" "Player 1")
                         (sc/score-score-card 4 3)          ;1st open frame
                         (sc/score-score-card 10 0)         ;strike
                         (sc/score-score-card 7 3)          ;spare
                         )]
      (is (= {:label "X", :running-score 27} (-> score-card :players (first) :frames (get 1))) "running score updated for last strike")
      (is (= {:label "7 /"} (-> score-card :players (first) :frames (last))) "label is 7 / and no running score")))

  (testing "two strikes in a row after spare"
    (let [score-card (-> (sc/initial-score-card "New Game" "Player 1")
                         (sc/score-score-card 4 3)          ;1st open frame
                         (sc/score-score-card 10 0)         ;strike
                         (sc/score-score-card 7 3)          ;spare
                         (sc/score-score-card 10 0)         ;strike
                         (sc/score-score-card 10 0)         ;strike
                         )]
      (is (= {:label "7 /", :running-score 47} (-> score-card :players (first) :frames (get 2))) "running score is updated for last spare")
      (is (= {:label "X"} (-> score-card :players (first) :frames (get 3))) "label is X and no running score for 2nd last frame")
      (is (= {:label "X"} (-> score-card :players (first) :frames (last))) "label is X and no running score for last frame")))

  (testing "last frame (10th frame) is a strike"
    (let [score-card (-> (sc/initial-score-card "New Game" "Player 1")
                         (sc/score-score-card 4 3)          ;1st open frame
                         (sc/score-score-card 10 0)         ;strike
                         (sc/score-score-card 7 3)          ;spare
                         (sc/score-score-card 10 0)         ;strike
                         (sc/score-score-card 10 0)         ;strike
                         (sc/score-score-card 4 5)          ;open
                         (sc/score-score-card 2 3)          ;open
                         (sc/score-score-card 7 1)          ;open
                         (sc/score-score-card 4 0)          ;open
                         (sc/score-score-card 10 0)         ;strike
                         )]
      (is (= 10 (-> score-card :players (first) :frames (count))) "10 frames created")
      (is (= {:label "X"} (-> score-card :players (first) :frames (last))) "last frame is not scored")))

  (testing "fill ball"
    (let [score-card (-> (sc/initial-score-card "New Game" "Player 1")
                         (sc/score-score-card 4 3)          ;1st open frame
                         (sc/score-score-card 10 0)         ;strike
                         (sc/score-score-card 7 3)          ;spare
                         (sc/score-score-card 10 0)         ;strike
                         (sc/score-score-card 10 0)         ;strike
                         (sc/score-score-card 4 5)          ;open
                         (sc/score-score-card 2 3)          ;open
                         (sc/score-score-card 7 1)          ;open
                         (sc/score-score-card 4 0)          ;open
                         (sc/score-score-card 10 0)         ;strike
                         (sc/score-score-card 3 6)          ;strike
                         )]
      (is (= 10 (-> score-card :players (first) :frames (count))) "No additional frame created")
      (is (= {:label "X", :running-score 135} (-> score-card :players (first) :frames (last))) "last frame is scored")))

  ;todo : teat spare in last frame
  ;todo : test for 2 players
  )

(deftest final-score
  ;todo
  )