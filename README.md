# ten-pin-bowling
API that implements a ten-pin bowling scorecard. 

## Usage
Following 3 functions are there

1. initial-score-card - To initialise the score card
2. score-score-card - Score the score card
3. final-score - Get the final score

## Tools
Build tool Leiningen (https://leiningen.org/) required.

## Test
Go to the root folder and execute the following command to run the test cases
```
lein test
```

Test can be executed in REPL too. If you are using an IDE and having REPL running, run the following commands

```
(in-ns 'ten-pin-bowling.score-card-test)
(run-tests)
```
## License

Copyright © 2021 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
