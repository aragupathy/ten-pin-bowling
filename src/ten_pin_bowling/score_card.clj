(ns ten-pin-bowling.score-card
  (:require [clojure.spec.alpha :as spec]
            [clojure.string :as str]
            [clojure.string :as s]))

(spec/def ::name (spec/and string? (comp not str/blank?)))
(spec/def ::players-name (spec/coll-of str :kind coll? :distinct true :min-count 1 :max-count 10))
(spec/def ::frame (spec/keys :req-un [::label] :opt-un [::running-score]))
(spec/def ::frames (spec/coll-of ::frame :kind vector? :max-count 10))
(spec/def ::player (spec/keys :req-un [::name ::frames ::total-score]))
(spec/def ::players (spec/coll-of ::player :kind vector? :distinct true :min-count 1 :max-count 10))
(spec/def ::game-name ::name)
(spec/def ::current-player-index (spec/and int? #(<= 0 % 9)))
(spec/def ::score-card (spec/keys :req-un [::game-name ::current-player-index ::players]))

(defn initial-score-card
  "game name and players name required to create the initial scoreboard"
  [game-name & players]
  (cond (not (spec/valid? ::name game-name)) (ex-info "Invalid game name"
                                                      {:details (spec/explain-str ::name game-name)})
        (not (spec/valid? ::players-name players)) (ex-info "Invalid players list"
                                                            {:details (spec/explain-str ::players players)})
        :else {:game-name            game-name
               :current-player-index 0
               :players              (vec (map #(assoc {} :name % :frames [] :total-score 0) players))}))

(defn- strike?
  ([result-of-ball-1 result-of-ball-2]
   (if (= 10 result-of-ball-1) true false))
  ([frame]
   (if (= "X" (:label frame)) true false)))

(defn- spare?
  ([result-of-ball-1 result-of-ball-2]
   (if (= 10 (+ result-of-ball-1 result-of-ball-2)) true false))
  ([frame]
   (if (s/includes? (:label frame) "/") true false)))

(defn- score-second-last-frame
  "if the last 2 frames are strike score of the 2nd last have to be calculated"
  [frames result-of-ball-1]
  (if (and (>= (count frames) 2)                            ;has more than 2 frames
           (strike? (get frames (- (count frames) 1)))      ;last frame is a strike
           (strike? (get frames (- (count frames) 2))))     ;2nd last frame is a strike
    (let [last-score (if (>= (count frames) 3) (:running-score (get frames (- (count frames) 3))) 0)]
      (assoc frames (- (count frames) 2)
                    (assoc (get frames (- (count frames) 2)) :running-score (+ last-score 10 10 result-of-ball-1))))
    frames))

(defn- score-last-frame
  "if the last frame is strike or spare the score of the frame have to be calculated"
  [frames result-of-ball-1 result-of-ball-2]
  (let [last-score (if (>= (count frames) 2) (:running-score (get frames (- (count frames) 2))) 0)]
    (cond (or (< (count frames) 1)                          ;no frames exist
              (and (strike? (last frames))
                   (strike? result-of-ball-1 result-of-ball-2)
                   (not (= 10 (count frames))))) frames     ;both strike and not fill ball need to wait to score
          (strike? (last frames)) (assoc frames (- (count frames) 1)
                                                (assoc (last frames)
                                                  :running-score (+ last-score 10 result-of-ball-1 result-of-ball-2)))
          (spare? (last frames)) (assoc frames (- (count frames) 1)
                                               (assoc (last frames)
                                                 :running-score (+ last-score 10 result-of-ball-1)))
          :else frames)))

(defn- add-new-frame
  "add the new frame for last throws"
  [frames result-of-ball-1 result-of-ball-2]
  (let [last-score (if (>= (count frames) 1) (:running-score (get frames (- (count frames) 1))) 0)
        frame (cond (= 10 (count frames)) nil
                    (strike? result-of-ball-1 result-of-ball-2) {:label "X"}
                    (spare? result-of-ball-1 result-of-ball-2) {:label (str result-of-ball-1 " " "/")}
                    :else {:label         (str result-of-ball-1 " " result-of-ball-2)
                           :running-score (+ last-score result-of-ball-1 result-of-ball-2)})]
    (if frame (conj frames frame) frames)))

(defn- total-score-of-frames
  "calculate the total of given frames"
  [frames]
  (reduce #(if (:running-score %2) (reduced (:running-score %2)) 0) 0 (reverse frames)))

(defn score-score-card
  "sore the given score card for the given results"
  [score-card result-of-ball-1 result-of-ball-2]
  ;;TODO: validate the score card
  (cond (not (<= 0 (+ result-of-ball-1 result-of-ball-2) 10))
            (ex-info "Invalid value passe for result-of-ball-2" {:result-of-ball-2 result-of-ball-2})
        :else (let [current-player-index (:current-player-index score-card)
                    current-player (get (:players score-card) current-player-index)
                    frames (-> current-player
                               :frames
                               (score-second-last-frame result-of-ball-1)
                               (score-last-frame result-of-ball-1 result-of-ball-2)
                               (add-new-frame result-of-ball-1 result-of-ball-2))
                    current-player (assoc current-player
                                     :frames frames
                                     :total-score (total-score-of-frames frames))]
                (assoc score-card
                  :players (assoc (:players score-card) current-player-index current-player)
                  :current-player-index (if (= (- (count (:players score-card)) 1) current-player-index)
                                          0 (+ 1 current-player-index))))))

(defn- is-frames-complete?
  [frames]
  (if (and (= 10 (count frames)) (:running-score (last frames))) true false))

(defn- is-complete?
  "Each player should have 10 frames and last frame should have running score for the game to complete"
  [score-card]
  (reduce #(if (is-frames-complete? (:frames %2)) true (reduced false)) true (:players score-card)))

(defn final-score
  "test whether the game is completed and returns the final score, returns nil if not completed"
  [score-card]
  (if (is-complete? score-card) (:players score-card) nil))